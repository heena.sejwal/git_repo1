#! /bin/bash

for pkg; do
	dpkg -s "$pkg" >/dev/null 2>&1 && {
		echo "$pkg is already installed."
	} || {
		sudo apt install -y $pkg
	}
done
